#!/bin/sh

get_tag_for_commit() {
    local repo="$1/.git"
    local CID=$2
    local tagfile

    for tagfile in `ls -1 $repo/refs/tags`; do
	TID=`cat $repo/refs/tags/$tagfile`
	if [ $TID = $CID ]; then
	    echo "$tagfile"
	    return 0
	fi
    done
    return 1
}

is_detached() {
    local repo="$1/.git"
    cat $repo/HEAD | grep -q 'ref:' $head
    return $?
}

is_dirty() {
    local root="$1"
    local repo="$root/.git"
    local tmp
    local cwd=`pwd`
    cd $root
    tmp=`git status -s`
    rc=$?
    cd $cwd
    if [ ! $rc -eq 0 ]; then
	return -1
    elif [ -z "$tmp" ]; then
	return 0
    else
	return 1
    fi
}

dirty_str() {
    local root="$1"
    local rc
    
    is_dirty "$root"
    rc="$?"
    case $rc in
    0)
	echo ""
	;;
    1)
	echo "+"
	;;
    *)
	echo "?"
	;;
    esac
}

get_current_branch_or_tag() {
    local root="$1"
    local repo="$root/.git"
    local cid
    local branch_or_tag

    is_detached $root
    if [ ! $? -eq 0 ]; then
	cid=`cat $repo/HEAD`
	branch_or_tag=`get_tag_for_commit $cid`
	if [ -z "$branch_or_tag" ]; then
	    branch_or_tag='?'
	fi
    else
	branch_or_tag=`cat $repo/HEAD | sed -n -r -e 's/^(ref:[ ]+.*\/)([^\/]+)$/\2/p'`
	if [ ! $? -eq 0 -o -z "$branch_or_tag" ]; then
	    branch_or_tag='?'
	fi

    fi
    echo "$branch_or_tag"
}

get_current_commit() {
    local root="$1"
    local repo="$root/.git"
    local refname
    local cid

    is_detached $root
    if [ ! $? -eq 0 ]; then
	echo `cat $repo/HEAD`
    else
	refpath=`cat $repo/HEAD | sed -n -r -e 's/^(ref:[ ]+)(.*)$/\2/p'`
	cid=`cat $repo/$refpath`
	if [ ! $? -eq 0 ]; then
	    return 1
	else
	    echo "$cid"
	fi
    fi
}

#set -x

#
# Find the repo in the current dir or a parent, or abort.
#
CWD=`pwd`
ROOT=$CWD
REPO=""
while [ 1 ]; do
    if [ ! -d $ROOT/.git ]; then
	cd ..
	NROOT=`pwd`
	if [ "$ROOT" = "$NROOT" ]; then
	    cd $CWD
	    (>&2 echo "ERROR: could not find git repo in parent of $CWD; cannot generate version.go!")
	    exit 1
	fi
	ROOT="$NROOT"
    else
	REPO="$ROOT/.git"
	break
    fi
done
cd $CWD

REFNAME=`get_current_branch_or_tag $ROOT`
REF=`get_current_commit $ROOT`
SHORTREF=`echo $REF | cut -c -8`
DIRTY=`dirty_str $ROOT`
DT=`date '+%Y%m%d-%H%M%S'`

BUILDID="${DT}/${REFNAME}@${SHORTREF}${DIRTY}"
VERSION=""
is_dirty $ROOT
if [ $? -eq 0 ]; then
    VERSION=`get_tag_for_commit $ROOT $REF`
fi
if [ -z "$VERSION" ]; then
    VERSION=`date '+%Y%m%d%H'`
fi

cat <<EOF
package main

const (
    Version = "$VERSION"
    BuildId = "$BUILDID"
)
EOF

exit 0
